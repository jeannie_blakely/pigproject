Week 3:
I felt like the instance variable maximumRolls along with its methods were unnecessary after implementing
the Strategies classes (the rollsTaken is a more nuanced substitute).  I commented these out of ComputerPlayer class
and also the setter in ComputerPane (in the handler).

I also missed a bug in last weeks assignment.  When the ComputerPlayer wins, the program doesn't disable.  The
HumanPlayer is able to continue scoring (and can even win if it makes it to 100 without hitting a Pig and triggering
the isGameOver check) after the ComputerPlayer hits the 100 mark.  Because the isGameOver method is only checking
the currentPlayer's total, it can potentially return inaccurate results.  I changed it to check the total of the ComputerPlayer
and the HumanPlayer explicitly to make sure either player reaching 100 would return true:

if (this.getComputerPlayer().getTotal() >= GOAL_SCORE || this.getHumanPlayer().getTotal() >= GOAL_SCORE) {
			return true;
		}

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Week 2:
There was a bug in the program that caused the turnTotal method to not reset after each player's turn (it
was in an if statement in the play method in the Game class).  This if statement reliably applied to the 
ComputerPlayer who always returns a !isMyTurn after the takeTurn method but did not necessarily apply to the
HumanPlayer which would return isMyTurn if the human had a successful turn and was "holding".  Since the resetTurnTotal
was happening after the hold method (and thus the swap to HumanPlayer) this has the effect of always resetting the
HumanPlayer turnTotal, but not necessarily the ComputerPlayer's.  If the ComputerPlayer had a successful turn 
with points added to total and the HumanPlayer had a successful turn, the turnTotal points were not resetting
on the ComputerPlayer's turn which was also causing inaccuracies in the ComputerPlayer Total in some instances. 
I moved the resetTurnTotal method to the hold method which both players use as a player swap, basically resetting
the turnTotal after the player swap ensuring that both players have their turnTotals reset.

From play in the Game class:
if (!this.currentPlayerObject.getValue().getIsMyTurn()) {		
			this.hold();
			// moved this method into hold method so each player gets turnTotal reset
			// this.currentPlayerObject.getValue().resetTurnTotal();
			
To hold in the Game class:
public void hold() {
		// Swap whose turn it is.
		this.swapWhoseTurn();
		// moved from play method to try to fix turnTotal bug <jbb>
		this.currentPlayerObject.getValue().resetTurnTotal();
		this.isGameOver();
	}
	
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Week 1:
In ComputerPane class:
The logic in the invalidated method confused me.  The starter code was set to:

  if (!myTurn) {
			// TODO: Set the user interface to show the results
			// of the computer rolling the dice

		} 

I could not figure out how to make this work (ie why would the interface be set to only show results when it 
*wasn't* the computer's turn??).  I took out the ! in front of myTurn to get it to display.

In AbstractPlayer:
I added two setter methods setIsMyTurn and setTotal to make ComputerPlayer functional (it needed to set the 
total to 0 in the constructor and set the isMyTurn to false  in the Overridden takeTurn method