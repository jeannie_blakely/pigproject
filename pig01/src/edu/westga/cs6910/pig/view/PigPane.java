package edu.westga.cs6910.pig.view;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.Game;
import edu.westga.cs6910.pig.model.HumanPlayer;
import edu.westga.cs6910.pig.model.Player;
import edu.westga.cs6910.pig.model.strategies.CautiousStrategy;
import edu.westga.cs6910.pig.model.strategies.RandomStrategy;
import edu.westga.cs6910.pig.model.strategies.GreedyStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

/**
 * Defines a GUI for the Pig game.
 * This class was started by CS6910
 * @author JEANNIE BLAKELY
 * @version 5-31-18
 */
public class PigPane extends BorderPane {
	private Game theGame;
	private BorderPane pnContent;
	private HumanPane pnHumanPlayer;
	private ComputerPane pnComputerPlayer;
	private StatusPane pnGameInfo;
	private Pane pnChooseFirstPlayer;
	// firstPlayer keeps track of who goes first: 0 (new game - chooser is enabled), 
	// 1 (computer first - chooser disabled) or 2 (human first - chooser disabled)
	private int firstPlayer;
	
	/**
	 * Creates a pane object to provide the view for the specified
	 * Game model object.
	 * 
	 * @param theGame	the domain model object representing the Pig game
	 * 
	 * @requires theGame != null
	 * @ensures	 the pane is displayed properly
	 */
	public PigPane(Game theGame) {
		this.theGame = theGame;
		
		this.firstPlayer = 0;
		
		this.pnContent = new BorderPane();
		
		this.addFirstPlayerChooserPane(theGame);
		
		this.addCenterPane();
		
		this.addMenuPane();
		
		this.setCenter(this.pnContent);
		
	}
	
	private void addFirstPlayerChooserPane(Game theGame) {
		HBox topBox = new HBox();
		topBox.getStyleClass().add("pane-border");	
		this.pnChooseFirstPlayer = new NewGamePane(theGame);
		topBox.getChildren().add(this.pnChooseFirstPlayer);
		this.pnContent.setCenter(topBox);
	}
	
	private void addCenterPane() {
		
		BorderPane centerPane = new BorderPane();
		//  Using the 'first player chooser pane' as a guide
		//  Create an HBox with the appropriate style, then make a human
		//	player pane and add it to the HBox. Finally add the HBox to the content pane
		HBox leftSideBox = new HBox();
		leftSideBox.getStyleClass().add("pane-border");
		this.pnHumanPlayer = new HumanPane(this.theGame);
		leftSideBox.getChildren().add(this.pnHumanPlayer);
		centerPane.setLeft(leftSideBox);
	
		// Using the other panes as a guide, create and add a status pane	
		HBox centerBox = new HBox();
		centerBox.getStyleClass().add("pane-border");
		this.pnGameInfo = new StatusPane(this.theGame);
		centerBox.getChildren().add(this.pnGameInfo);
		centerPane.setCenter(centerBox);
		
		// Using the other panes as a guide, create and add a computer pane
		HBox rightSideBox = new HBox();
		rightSideBox.getStyleClass().add("pane-border");
		this.pnComputerPlayer = new ComputerPane(this.theGame);
		rightSideBox.getChildren().add(this.pnComputerPlayer);
		centerPane.setRight(rightSideBox);
		
		this.pnContent.setBottom(centerPane);
		this.restartPlayerSettings(); 
	}
	
	/*
	 * Draws a borderpane and creates a menu and goalScore setter and restartGame box 
	 * to add to it
	 */
	private void addMenuPane() {
		BorderPane menuPane = new BorderPane();
		MenuBar menuBar = new MenuBar();
		menuBar.getMenus().addAll(this.buildFileMenu(), this.buildStrategyMenu());
		menuPane.setTop(menuBar);
		menuPane.setCenter(this.buildGoalSetterBox());
		menuPane.setRight(this.buildPlayAgainBox());
		this.pnContent.setTop(menuPane);
	}
	
	/**
	 * Creates the file menu 
	 */
	private Menu buildFileMenu() {
		Menu fileMenu = new Menu("_File");
		fileMenu.setMnemonicParsing(true);
		MenuItem exitMenuItem = new MenuItem("E_xit");
		exitMenuItem.setAccelerator(KeyCombination.keyCombination("SHORTCUT+X"));
		exitMenuItem.setOnAction(actionEvent -> System.exit(0));
		fileMenu.getItems().addAll(exitMenuItem);
		return fileMenu;
	}
	
	/**
	 * Creates the strategy menu 
	 */
	private Menu buildStrategyMenu() {
		Menu strategyMenu = new Menu("_Strategy");
		strategyMenu.setMnemonicParsing(true);
		MenuItem cautiousMenuItem = new MenuItem("_Cautious");
		cautiousMenuItem.setOnAction(actionEvent -> this.theGame.getComputerPlayer().setStrategy(new CautiousStrategy()));
		cautiousMenuItem.setMnemonicParsing(true);
		MenuItem greedyMenuItem = new MenuItem("_Greedy");
		greedyMenuItem.setOnAction(actionEvent -> this.theGame.getComputerPlayer().setStrategy(new GreedyStrategy()));
		greedyMenuItem.setMnemonicParsing(true);
		MenuItem randomMenuItem = new MenuItem("_Random");
		randomMenuItem.setOnAction(actionEvent -> this.theGame.getComputerPlayer().setStrategy(new RandomStrategy()));
		randomMenuItem.setMnemonicParsing(true);
		strategyMenu.getItems().addAll(cautiousMenuItem, greedyMenuItem, randomMenuItem);
		return strategyMenu;
	}
	
	/**
	 * Creates the goal setter HBox
	 */
	private HBox buildGoalSetterBox() {
		HBox goalBox = new HBox();
		this.theGame.addListener(ov -> {
			if (!this.theGame.isGameOver()) {
				goalBox.setDisable(true);
			} else {
				goalBox.setDisable(false);
			}
		});
		goalBox.setAlignment(Pos.TOP_CENTER);
		Label goalLabel = new Label("Goal Score: ");
		TextField goalText = new TextField("" + this.theGame.getGoalScore());
		goalText.setPrefWidth(50);
		Button setButton = new Button("Set Goal");
		setButton.setOnAction(e -> {
			PigPane.this.restartGame(PigPane.this.getTextInt(goalText.getText()));
			goalBox.setDisable(true);
			PigPane.this.addCenterPane();
		});
		goalBox.getChildren().addAll(goalLabel, goalText, setButton);
		return goalBox;
	}
	
	/**
	 * Creates the restart game button
	 */
	private HBox buildPlayAgainBox() {
		HBox restartBox = new HBox();
		this.theGame.addListener(ov -> {
			if (!this.theGame.isGameOver()) {
				restartBox.setDisable(true);
			} else {
				restartBox.setDisable(false);
			}
		});
		restartBox.setAlignment(Pos.TOP_RIGHT);
		Button restartButton = new Button("Play Again");
		restartButton.setOnAction(actionEvent -> this.restartGame(100));
		restartBox.getChildren().add(restartButton);
		return restartBox;
	}
	
	private void restartGame(int goalScore) {
		PigStrategy newPigStrategy = new CautiousStrategy();
		this.theGame = new Game(new HumanPlayer("Human"), new ComputerPlayer(newPigStrategy, goalScore), goalScore);
		this.pnContent = new BorderPane();
		this.addFirstPlayerChooserPane(this.theGame);
		this.addCenterPane();
		this.addMenuPane();
		this.setCenter(this.pnContent);
		this.restartPlayerSettings();
	}
	
	/* 
	 * Checks to see if firstPlayer is set (new game or a restart) and sets
	 * attributes of previously chosen player
	 */	
	private void restartPlayerSettings() {
		if (this.firstPlayer == 1) {
			this.pnHumanPlayer.setDisable(true);
			this.pnComputerPlayer.setDisable(false);
			this.pnChooseFirstPlayer.setDisable(true);
		} else if (this.firstPlayer == 2) {
			this.pnChooseFirstPlayer.setDisable(true);
			this.pnHumanPlayer.setDisable(false);
			this.pnComputerPlayer.setDisable(true);
		}
	}
	
	/**
	 * Parses input string from the TextField into int value
	 */
	private int getTextInt(String input) {
		int value = 0;
		try {
			value = Integer.parseInt(input);
		} catch (NumberFormatException nfe) {
			
		}
		return value;
	}

	/*
	 * Defines the panel in which the user selects which Player plays first.
	 */
	private final class NewGamePane extends GridPane {
		private RadioButton radHumanPlayer;
		private RadioButton radComputerPlayer;
		private RadioButton radRandomPlayer;
		
		private Game theGame;
		private Player theHuman;
		private Player theComputer;

		private NewGamePane(Game theGame) {
			this.theGame = theGame;
			
			this.theHuman = this.theGame.getHumanPlayer();
			this.theComputer = this.theGame.getComputerPlayer();
			
			this.buildPane();
			
		}
		
		private void buildPane() {
			this.setHgap(20);
			this.setVgap(8);
			this.radRandomPlayer = new RadioButton("Random Player first");
			this.radRandomPlayer.setOnAction(e -> {
				if (Math.random() < 0.5) {
					NewGamePane.this.radHumanPlayer.fire();
				} else {
					NewGamePane.this.radComputerPlayer.fire();
				}
			});
			
			this.radHumanPlayer = new RadioButton(this.theHuman.getName() + " first");	
			this.radHumanPlayer.setOnAction(new HumanFirstListener());
			// Instantiate the computer player button and add 
			// ComputerFirstListener as its action listener.
			this.radComputerPlayer = new RadioButton(this.theComputer.getName() + " first");
			this.radComputerPlayer.setOnAction(new ComputerFirstListener());
			// Create a ToggleGroup and add the 2 radio buttons to it.
			ToggleGroup radGroup = new ToggleGroup();
			this.radHumanPlayer.setToggleGroup(radGroup);
			this.radComputerPlayer.setToggleGroup(radGroup);
			this.radRandomPlayer.setToggleGroup(radGroup);
			
			// Add the 3 radio buttons to this pane.
			NewGamePane.setConstraints(this.radHumanPlayer, 1, 1);
			NewGamePane.setConstraints(this.radComputerPlayer, 2, 1);
			NewGamePane.setConstraints(this.radRandomPlayer, 3, 1);
			getChildren().addAll(this.radHumanPlayer, this.radComputerPlayer, this.radRandomPlayer);
			this.newGameOrRestart();
		}
		
		/* 
		 * Checks to see if firstPlayer is set (new game or a restart) and sets
		 * previously choosen player
		 */	
		private void newGameOrRestart() {
			if (PigPane.this.firstPlayer == 1) {
				NewGamePane.this.radComputerPlayer.fire();
				PigPane.this.theGame.startNewGame(NewGamePane.this.theComputer);
			} else if (PigPane.this.firstPlayer == 2) {
				NewGamePane.this.radHumanPlayer.fire();
				PigPane.this.theGame.startNewGame(NewGamePane.this.theHuman);
			}
		}
		
		/* 
		 * Defines the listener for computer player first button.
		 */		
		private class ComputerFirstListener implements EventHandler<ActionEvent> {
			@Override
			/** 
			 * Enables the ComputerPlayerPanel and starts a new game. 
			 * Event handler for a click in the computerPlayerButton.
			 */
			public void handle(ActionEvent arg0) {
				PigPane.this.pnComputerPlayer.setDisable(false);
				PigPane.this.pnChooseFirstPlayer.setDisable(true);
				PigPane.this.theGame.startNewGame(NewGamePane.this.theComputer);
				PigPane.this.firstPlayer = 1;
			}
		}
		
		/* 
		 * Defines the listener for human player first button.
		 */	
		private class HumanFirstListener implements EventHandler<ActionEvent> {
			/* 
			 * Sets up user interface and starts a new game. 
			 * Event handler for a click in the human player button.
			 */
			@Override
			public void handle(ActionEvent event) {
				PigPane.this.pnChooseFirstPlayer.setDisable(true);
				// Enable the human player pane and start a game
				//	with the human playing first.
				PigPane.this.pnHumanPlayer.setDisable(false);
				PigPane.this.theGame.startNewGame(NewGamePane.this.theHuman);
				PigPane.this.firstPlayer = 2;

			}
		}
	}
}
