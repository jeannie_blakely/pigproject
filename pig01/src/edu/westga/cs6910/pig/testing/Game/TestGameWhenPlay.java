package edu.westga.cs6910.pig.testing.Game;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.Game;
import edu.westga.cs6910.pig.model.HumanPlayer;
import edu.westga.cs6910.pig.model.strategies.CautiousStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestGameWhenPlay {

	@Test
	public void testShouldProduceFailIfTotalAndIsGameOverDoNotAlignWithOver100() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		//newComputerPlayer.setMaximumRolls();
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newHumanPlayer);
		for (int count = 0; count < 10000; count++) {
			newGame.play();
			if ((newGame.getComputerPlayer().getTotal() >= 100 || newGame.getHumanPlayer().getTotal() >= 100) && !newGame.isGameOver()) {
				fail(newGame.getCurrentPlayer().getName() + " with total of " + newGame.getCurrentPlayer().getTotal() + " and isGameOver is " + newGame.isGameOver());
			}
		}
	}
	
	@Test
	public void testShouldProduceFailIfTotalAndIsGameOverDoNotAlignWithOver75WithGoalScoreConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 75);
		//newComputerPlayer.setMaximumRolls();
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 75);
		newGame.startNewGame(newHumanPlayer);
		for (int count = 0; count < 10000; count++) {
			newGame.play();
			if ((newGame.getComputerPlayer().getTotal() >= 75 || newGame.getHumanPlayer().getTotal() >= 75) && !newGame.isGameOver()) {
				fail(newGame.getCurrentPlayer().getName() + " with total of " + newGame.getCurrentPlayer().getTotal() + " and isGameOver is " + newGame.isGameOver());
			}
		}
	}
	
	@Test
	public void testShouldProduceFailIfTotalAndIsGameOverDoNotAlignWithOver163WithGoalScoreConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 163);
		//newComputerPlayer.setMaximumRolls();
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 163);
		newGame.startNewGame(newHumanPlayer);
		for (int count = 0; count < 10000; count++) {
			newGame.play();
			if ((newGame.getComputerPlayer().getTotal() >= 163 || newGame.getHumanPlayer().getTotal() >= 163) && !newGame.isGameOver()) {
				fail(newGame.getCurrentPlayer().getName() + " with total of " + newGame.getCurrentPlayer().getTotal() + " and isGameOver is " + newGame.isGameOver());
			}
		}
	}
}
