package edu.westga.cs6910.pig.testing.Game;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.Game;
import edu.westga.cs6910.pig.model.HumanPlayer;
import edu.westga.cs6910.pig.model.strategies.CautiousStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestGameStartNewGame {

	@Test
	public void testShouldProduceGameWithCurrentPlayerNameHuman() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newHumanPlayer);
		assertEquals("Human", newGame.getCurrentPlayer().getName());
	}
	
	@Test
	public void testShouldProduceGameWithCurrentPlayerNameSimpleComputer() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newComputerPlayer);
		assertEquals("Simple computer", newGame.getCurrentPlayer().getName());
	}
	
	@Test
	public void testShouldProduceGameWithCurrentPlayerNameHumanNewConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 50);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 50);
		newGame.startNewGame(newHumanPlayer);
		assertEquals("Human", newGame.getCurrentPlayer().getName());
	}
	
	@Test
	public void testShouldProduceGameWithCurrentPlayerNameSimpleComputerNewConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 75);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 75);
		newGame.startNewGame(newComputerPlayer);
		assertEquals("Simple computer", newGame.getCurrentPlayer().getName());
	}
}
