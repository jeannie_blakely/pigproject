package edu.westga.cs6910.pig.testing.Game;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.Game;
import edu.westga.cs6910.pig.model.HumanPlayer;
import edu.westga.cs6910.pig.model.strategies.CautiousStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestGameIsGameOver {
	
	/**
	 * No startGame method called to set CurrentPlayer which should
	 * be null.  Therefore isGameOver should be true    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverTrueBecauseCurrentPlayerNull() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		assertEquals(true, newGame.isGameOver());
	}
	
	/**
	 * startGame method called to set CurrentPlayer to
	 * HumanPlayer.  Therefore isGameOver should be false    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverFalseBecauseCurrentPlayerHumanPlayer() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newHumanPlayer);
		assertEquals(false, newGame.isGameOver());
	}
	
	/**
	 * startGame method called to set CurrentPlayer to
	 * ComputerPlayer.  Therefore isGameOver should be false    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverFalseBecauseCurrentPlayerComputerPlayer() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newComputerPlayer);
		assertEquals(false, newGame.isGameOver());
	}
	
	/**
	 * startGame method called to set CurrentPlayer to
	 * HumanPlayer.  Total for HumanPlayer set to 100
	 * Therefore isGameOver should be true    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverTrueBecauseCurrentPlayerHumanPlayerHasTotal100() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newHumanPlayer);
		newHumanPlayer.setTotal(100);
		assertEquals(true, newGame.isGameOver());
	}
	
	/**
	 * startGame method called to set CurrentPlayer to
	 * ComputerPlayer.  Total for ComputerPlayer set to 100
	 * Therefore isGameOver should be true    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverTrueBecauseComputerPlayerHasTotal100() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newComputerPlayer);
		newComputerPlayer.setTotal(100);
		assertEquals(true, newGame.isGameOver());
	}
	
	/**
	 * startGame method called to set CurrentPlayer to
	 * HumanPlayer.  Total for HumanPlayer set to 85
	 * Therefore isGameOver should be false    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverFalseBecauseCurrentPlayerHumanPlayerHasTotal85() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newHumanPlayer);
		newHumanPlayer.setTotal(85);
		assertEquals(false, newGame.isGameOver());
	}
	
	/**
	 * startGame method called to set CurrentPlayer to
	 * ComputerPlayer.  Total for ComputerPlayer set to 31
	 * Therefore isGameOver should be false    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverFalseBecauseCurrentPlayerComputerPlayerHasTotal31() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newComputerPlayer);
		newComputerPlayer.setTotal(31);
		assertEquals(false, newGame.isGameOver());
	}
	
	/**
	 * No startGame method called to set CurrentPlayer which should
	 * be null.  Therefore isGameOver should be true    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverTrueBecauseCurrentPlayerNullWith3ParamConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 75);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 75);
		assertEquals(true, newGame.isGameOver());
	}
	
	/**
	 * startGame method called to set CurrentPlayer to
	 * HumanPlayer.  Total for HumanPlayer set to 85
	 * Therefore isGameOver should be false    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverFalseBecauseCurrentPlayerHumanPlayerHasTotal85With3ParamConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 90);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 90);
		newGame.startNewGame(newHumanPlayer);
		newHumanPlayer.setTotal(85);
		assertEquals(false, newGame.isGameOver());
	}
	
	/**
	 * startGame method called to set CurrentPlayer to
	 * ComputerPlayer.  Total for ComputerPlayer set to 31
	 * And goalScore is 50.  Therefore isGameOver should be false    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverFalseBecauseCurrentPlayerComputerPlayerHasTotal31With3ParamConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 50);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 50);
		newGame.startNewGame(newComputerPlayer);
		newComputerPlayer.setTotal(31);
		assertEquals(false, newGame.isGameOver());
	}
	
	/**
	 * startGame method called to set CurrentPlayer to
	 * ComputerPlayer.  Total for ComputerPlayer set to 100
	 * Therefore isGameOver should be true    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverTrueBecauseComputerPlayerHasTotal100With3ParamConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, -5);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, -5);
		newGame.startNewGame(newComputerPlayer);
		newComputerPlayer.setTotal(100);
		assertEquals(true, newGame.isGameOver());
	}
	
	/**
	 * startGame method called to set CurrentPlayer to
	 * ComputerPlayer.  Therefore isGameOver should be false    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverFalseBecauseCurrentPlayerComputerPlayerWith3ParamConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 50);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 50);
		newGame.startNewGame(newComputerPlayer);
		assertEquals(false, newGame.isGameOver());
	}

}
