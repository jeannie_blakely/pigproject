package edu.westga.cs6910.pig.testing.Game;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.Game;
import edu.westga.cs6910.pig.model.HumanPlayer;
import edu.westga.cs6910.pig.model.strategies.CautiousStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;
import edu.westga.cs6910.pig.model.ComputerPlayer;

class TestGameGetComputerPlayer {

	@Test
	public void testShouldProduceGameWithComputerPlayerNameSimpleComputer() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		assertEquals("Simple computer", newGame.getComputerPlayer().getName());
	}
	
	@Test
	public void testShouldProduceGameWithComputerPlayerNameSimpleComputerWith3ParamConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 70);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 70);
		assertEquals("Simple computer", newGame.getComputerPlayer().getName());
	}
}
