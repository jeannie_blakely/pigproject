package edu.westga.cs6910.pig.testing.Game;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.Game;
import edu.westga.cs6910.pig.model.HumanPlayer;
import edu.westga.cs6910.pig.model.strategies.CautiousStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;
import edu.westga.cs6910.pig.model.ComputerPlayer;

class TestWhenGameIsCreated {

	@Test
	public void testShouldProduceGameWithGoalScore100Human0SimpleComputer0() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		assertEquals("Goal Score: 100\r\n" + "Human: 0\r\n" + "Simple computer: 0", newGame.toString());
	}
	
	@Test
	public void testShouldProduceGameWithGoalScore75Human0SimpleComputer0() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 75);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 75);
		assertEquals("Goal Score: 75\r\n" + "Human: 0\r\n" + "Simple computer: 0", newGame.toString());
	}
	
	@Test
	public void testShouldProduceGameWithGoalScore350Human0SimpleComputer0() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 350);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 350);
		assertEquals("Goal Score: 350\r\n" + "Human: 0\r\n" + "Simple computer: 0", newGame.toString());
	}
	
	/**
	 * Goal score resets to 100 because invalid number
	 */
	@Test
	public void testShouldProduceGameWithGoalScore100Human0SimpleComputer0InvalidNumber0() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 0);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 0);
		assertEquals("Goal Score: 100\r\n" + "Human: 0\r\n" + "Simple computer: 0", newGame.toString());
	}
	
	/**
	 * Goal score resets to 100 because invalid number
	 */
	@Test
	public void testShouldProduceGameWithGoalScore100Human0SimpleComputer0InvalidNumberNeg5() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, -5);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, -5);
		assertEquals("Goal Score: 100\r\n" + "Human: 0\r\n" + "Simple computer: 0", newGame.toString());
	}
}
