package edu.westga.cs6910.pig.testing.Game;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.Game;
import edu.westga.cs6910.pig.model.HumanPlayer;
import edu.westga.cs6910.pig.model.strategies.CautiousStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;
import edu.westga.cs6910.pig.model.ComputerPlayer;

class TestGameToString {

	@Test
	public void testShouldProduceGameWithGoalScore100Human100SimpleComputer0GameOverWinnerHuman() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.getHumanPlayer().setTotal(100);
		assertEquals("Goal Score: 100\r\n"
				+ "Human: 100\r\n"
				+ "Simple computer: 0\r\n"
				+ "Game over! Winner: Human", newGame.toString());
	}
	
	@Test
	public void testShouldProduceGameWithGoalScore100Human0SimpleComputer100GameOverWinnerComputer() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.getComputerPlayer().setTotal(100);
		assertEquals("Goal Score: 100\r\n"
				+ "Human: 0\r\n"
				+ "Simple computer: 100\r\n"
				+ "Game over! Winner: Simple computer", newGame.toString());
	}
	
	@Test
	public void testShouldProduceGameWithGoalScore75Human0SimpleComputer75GameOverWinnerComputer() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 75);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 75);
		newGame.getComputerPlayer().setTotal(75);
		assertEquals("Goal Score: 75\r\n"
				+ "Human: 0\r\n"
				+ "Simple computer: 75\r\n"
				+ "Game over! Winner: Simple computer", newGame.toString());
	}

	@Test
	public void testShouldProduceGameWithGoalScore31Human0SimpleComputer31GameOverWinnerComputer() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 31);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 31);
		newGame.getComputerPlayer().setTotal(31);
		assertEquals("Goal Score: 31\r\n"
				+ "Human: 0\r\n"
				+ "Simple computer: 31\r\n"
				+ "Game over! Winner: Simple computer", newGame.toString());
	}

	@Test
	public void testShouldProduceGameWithGoalScore100Human0SimpleComputer100GameOverWinnerComputerInvalidData() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 0);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 0);
		newGame.getComputerPlayer().setTotal(100);
		assertEquals("Goal Score: 100\r\n"
				+ "Human: 0\r\n"
				+ "Simple computer: 100\r\n"
				+ "Game over! Winner: Simple computer", newGame.toString());
	}
}
