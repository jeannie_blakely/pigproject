package edu.westga.cs6910.pig.testing.Game;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.Game;
import edu.westga.cs6910.pig.model.HumanPlayer;
import edu.westga.cs6910.pig.model.strategies.CautiousStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;
import edu.westga.cs6910.pig.model.ComputerPlayer;

class TestGameGetHumanPlayer {

	@Test
	public void testShouldProduceGameWithHumanPlayerNameHuman() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		assertEquals("Human", newGame.getHumanPlayer().getName());
	}
	
	@Test
	public void testShouldProduceGameWithHumanPlayerNameHumanoid() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Humanoid");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		assertEquals("Humanoid", newGame.getHumanPlayer().getName());
	}
	
	@Test
	public void testShouldProduceGameWithHumanPlayerNameHumanWith3ParamConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 18);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 18);
		assertEquals("Human", newGame.getHumanPlayer().getName());
	}
	
	@Test
	public void testShouldProduceGameWithHumanPlayerNameHumanoidWith3ParamConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Humanoid");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 20);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 20);
		assertEquals("Humanoid", newGame.getHumanPlayer().getName());
	}

}
