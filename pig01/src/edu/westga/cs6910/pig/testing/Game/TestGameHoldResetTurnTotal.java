package edu.westga.cs6910.pig.testing.Game;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.Game;
import edu.westga.cs6910.pig.model.HumanPlayer;
import edu.westga.cs6910.pig.model.strategies.CautiousStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestGameHoldResetTurnTotal {

	@Test
	public void testShouldPassIfAllComputerPlayerObjectAreResetToTurnTotal0() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		//newComputerPlayer.setMaximumRolls();
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newComputerPlayer);
		for (int count = 0; count < 10000; count++) {
			newGame.play();
			newGame.hold();
			if (newGame.getComputerPlayer().getTurnTotal() != 0) {
				fail(newGame.getComputerPlayer().getName() + " with turnTotal of " + newGame.getComputerPlayer().getTurnTotal());
			}
		}
	}
	
	@Test
	public void testShouldPassIfAllHumanPlayerObjectsAreResetToTurnTotal0() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		//newComputerPlayer.setMaximumRolls();
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newComputerPlayer);
		for (int count = 0; count < 10000; count++) {
			newGame.play();
			newGame.hold();
			if (newGame.getHumanPlayer().getTurnTotal() != 0) {
				fail(newGame.getHumanPlayer().getName() + " with turnTotal of " + newGame.getHumanPlayer().getTurnTotal());
			}
		}
	}
	
	@Test
	public void testShouldPassIfAllComputerPlayerObjectAreResetToTurnTotal0With3ParamConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 25);
		//newComputerPlayer.setMaximumRolls();
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 25);
		newGame.startNewGame(newComputerPlayer);
		for (int count = 0; count < 10000; count++) {
			newGame.play();
			newGame.hold();
			if (newGame.getComputerPlayer().getTurnTotal() != 0) {
				fail(newGame.getComputerPlayer().getName() + " with turnTotal of " + newGame.getComputerPlayer().getTurnTotal());
			}
		}
	}
	
	@Test
	public void testShouldPassIfAllHumanPlayerObjectsAreResetToTurnTotal0With3ParamConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 80);
		//newComputerPlayer.setMaximumRolls();
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 80);
		newGame.startNewGame(newComputerPlayer);
		for (int count = 0; count < 10000; count++) {
			newGame.play();
			newGame.hold();
			if (newGame.getHumanPlayer().getTurnTotal() != 0) {
				fail(newGame.getHumanPlayer().getName() + " with turnTotal of " + newGame.getHumanPlayer().getTurnTotal());
			}
		}
	}

}
