package edu.westga.cs6910.pig.testing.Game;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import edu.westga.cs6910.pig.model.Game;
import edu.westga.cs6910.pig.model.HumanPlayer;
import edu.westga.cs6910.pig.model.strategies.CautiousStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;
import edu.westga.cs6910.pig.model.ComputerPlayer;

class TestGameGetDicePair {

	@Test
	public void testShouldProduceGameWithDie1ValueOf1() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		assertEquals(1, newGame.getDicePair().getDie1Value());
	}
	
	@Test
	public void testShouldProduceGameWithDie2ValueOf1() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		assertEquals(1, newGame.getDicePair().getDie2Value());
	}
	
	@Test
	public void testShouldProduceGameWithDie1ValueOf1With3ParamConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 180);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 180);
		assertEquals(1, newGame.getDicePair().getDie1Value());
	}
	
	@Test
	public void testShouldProduceGameWithDie2ValueOf1With3ParamConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 75);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 75);
		assertEquals(1, newGame.getDicePair().getDie2Value());
	}

}
