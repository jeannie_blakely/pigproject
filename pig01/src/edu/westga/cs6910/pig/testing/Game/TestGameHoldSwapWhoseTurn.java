package edu.westga.cs6910.pig.testing.Game;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.Game;
import edu.westga.cs6910.pig.model.HumanPlayer;
import edu.westga.cs6910.pig.model.strategies.CautiousStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestGameHoldSwapWhoseTurn {

	@Test
	public void testShouldProduceGameWithCurrentPlayerComputerBecauseHoldSwitchedPlayers() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newHumanPlayer);
		newGame.hold();
		assertEquals("Simple computer", newGame.getCurrentPlayer().getName());
	}
	
	@Test
	public void testShouldProduceGameWithCurrentPlayerHumanBecauseHoldSwitchedPlayers() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newComputerPlayer);
		newGame.hold();
		assertEquals("Human", newGame.getCurrentPlayer().getName());
	}
	
	@Test
	public void testShouldProduceGameWithCurrentPlayerComputerBecauseHoldSwitchedPlayersWith3ParamConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 25);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 25);
		newGame.startNewGame(newHumanPlayer);
		newGame.hold();
		assertEquals("Simple computer", newGame.getCurrentPlayer().getName());
	}
	
	@Test
	public void testShouldProduceGameWithCurrentPlayerHumanBecauseHoldSwitchedPlayersWith3ParamConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 80);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 80);
		newGame.startNewGame(newComputerPlayer);
		newGame.hold();
		assertEquals("Human", newGame.getCurrentPlayer().getName());
	}

}
