package edu.westga.cs6910.pig.testing.ComputerPlayer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.strategies.CautiousStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestComputerPlayerGetTurnTotal {

	@Test
	void testShouldProduceComputerPlayerWithTurnTotal0() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		assertEquals(0, newComputerPlayer.getTurnTotal());
	}
	
	@Test
	void testShouldProduceComputerPlayerWithTurnTotal0With2ParamConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 69);
		assertEquals(0, newComputerPlayer.getTurnTotal());
	}
}
