package edu.westga.cs6910.pig.testing.ComputerPlayer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.strategies.CautiousStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestComputerPlayerGetName {

	@Test
	public void testShouldProduceComputerPlayerWithName() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		assertEquals("Simple computer", newComputerPlayer.getName());
	}
	
	@Test
	public void testShouldProduceComputerPlayerWithNameWith2ParamConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 650);
		assertEquals("Simple computer", newComputerPlayer.getName());
	}

}
