package edu.westga.cs6910.pig.testing.ComputerPlayer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.strategies.CautiousStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestComputerPlayerSetIsMyTurn {

	@Test
	public void testSetIsMyTurnShouldBeTrue() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		newComputerPlayer.setIsMyTurn(true);
		assertEquals(true, newComputerPlayer.getIsMyTurn());
	}
	
	@Test
	public void testSetIsMyTurnShouldBeFalse() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		newComputerPlayer.setIsMyTurn(false);
		assertEquals(false, newComputerPlayer.getIsMyTurn());
	}
	
	@Test
	public void testSetIsMyTurnShouldBeTrueWith2ParamConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 213);
		newComputerPlayer.setIsMyTurn(true);
		assertEquals(true, newComputerPlayer.getIsMyTurn());
	}
	
	@Test
	public void testSetIsMyTurnShouldBeFalseWith2ParamConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 165);
		newComputerPlayer.setIsMyTurn(false);
		assertEquals(false, newComputerPlayer.getIsMyTurn());
	}

}
