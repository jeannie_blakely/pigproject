package edu.westga.cs6910.pig.testing.ComputerPlayer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.strategies.CautiousStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestComputerPlayerSetTotal {

	@Test
	public void testShouldProduceComputerPlayerWithTotal0() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		newComputerPlayer.setTotal(0);
		assertEquals(0, newComputerPlayer.getTotal());
	}
	
	@Test
	public void testShouldProduceComputerPlayerWithTotal89() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		newComputerPlayer.setTotal(89);
		assertEquals(89, newComputerPlayer.getTotal());
	}
	
	@Test
	public void testGoalScoreConstructorShouldProduceComputerPlayerWithTotal89() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		newComputerPlayer.setTotal(89);
		assertEquals(89, newComputerPlayer.getTotal());
	}
	
	@Test
	public void testShouldProduceComputerPlayerWithTotal0With2ParamConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 695);
		newComputerPlayer.setTotal(0);
		assertEquals(0, newComputerPlayer.getTotal());
	}
	
	@Test
	public void testShouldProduceComputerPlayerWithTotal89With2ParamConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 95);
		newComputerPlayer.setTotal(89);
		assertEquals(89, newComputerPlayer.getTotal());
	}
	
	@Test
	public void testGoalScoreConstructorShouldProduceComputerPlayerWithTotal89With2ParamConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 21);
		newComputerPlayer.setTotal(89);
		assertEquals(89, newComputerPlayer.getTotal());
	}
}
