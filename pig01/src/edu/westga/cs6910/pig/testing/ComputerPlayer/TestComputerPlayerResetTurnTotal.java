package edu.westga.cs6910.pig.testing.ComputerPlayer;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.strategies.CautiousStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestComputerPlayerResetTurnTotal {

	@Test
	public void testShouldFailIfResetTurnTotalDoesNotSetTurnTotalValueTo0() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		//newComputerPlayer.setMaximumRolls();
		for (int count = 0; count < 10000; count++) {
			newComputerPlayer.takeTurn();
			newComputerPlayer.resetTurnTotal();
			if (newComputerPlayer.getTurnTotal() != 0) {
				fail("Computer Player with turnTotal of " + newComputerPlayer.getTurnTotal() + " after resetTurnTotal");
			}
		}
	}
	
	@Test
	public void testShouldFailIfResetTurnTotalDoesNotSetTurnTotalValueTo0With2ParamConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 95);
		//newComputerPlayer.setMaximumRolls();
		for (int count = 0; count < 10000; count++) {
			newComputerPlayer.takeTurn();
			newComputerPlayer.resetTurnTotal();
			if (newComputerPlayer.getTurnTotal() != 0) {
				fail("Computer Player with turnTotal of " + newComputerPlayer.getTurnTotal() + " after resetTurnTotal");
			}
		}
	}
}
