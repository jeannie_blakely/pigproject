package edu.westga.cs6910.pig.testing.ComputerPlayer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;
import edu.westga.cs6910.pig.model.strategies.CautiousStrategy;

class TestComputerPlayerGetDiceValues {

	@Test
	public void testShouldProduceComputerPlayerWithDiceValues1Comma1() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		assertEquals("1, 1", newComputerPlayer.getDiceValues());
	}
	
	@Test
	public void testShouldProduceComputerPlayerWithDiceValues1Comma1With2ParamConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 75);
		assertEquals("1, 1", newComputerPlayer.getDiceValues());
	}
}
