package edu.westga.cs6910.pig.testing.ComputerPlayer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.strategies.CautiousStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestComputerPlayerGetIsMyTurn {

	@Test
	public void testShouldProduceComputerPlayerWithIsMyTurnFalse() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		assertEquals(false, newComputerPlayer.getIsMyTurn());
	}
	
	@Test
	public void testShouldProduceComputerPlayerWithIsMyTurnFalseWith2ParamConstructor() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 60);
		assertEquals(false, newComputerPlayer.getIsMyTurn());
	}

}
