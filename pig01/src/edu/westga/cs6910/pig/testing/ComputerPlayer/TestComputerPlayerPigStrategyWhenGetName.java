package edu.westga.cs6910.pig.testing.ComputerPlayer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.strategies.CautiousStrategy;
import edu.westga.cs6910.pig.model.strategies.GreedyStrategy;
import edu.westga.cs6910.pig.model.strategies.RandomStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestComputerPlayerPigStrategyWhenGetName {

	@Test
	public void testShouldReturnCautious() {
		PigStrategy newPigStrategy = new CautiousStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		assertEquals("Cautious", newComputerPlayer.getStrategy().getName());
	}
	
	@Test
	public void testShouldReturnGreedy() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		assertEquals("Greedy", newComputerPlayer.getStrategy().getName());
	}
	
	@Test
	public void testShouldReturnRandom() {
		PigStrategy newPigStrategy = new RandomStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		assertEquals("Random", newComputerPlayer.getStrategy().getName());
	}
	
	@Test
	public void testShouldReturnRandomWith2ParamConstructor() {
		PigStrategy newPigStrategy = new RandomStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 85);
		assertEquals("Random", newComputerPlayer.getStrategy().getName());
	}
}
