package edu.westga.cs6910.pig.testing.die;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import java.util.Random;

import edu.westga.cs6910.pig.model.Die;

class TestWhenDieIsCreated {

	@Test
	public void testShouldProduceDieWith1Pip() {
		Die newDie = new Die(new Random());
		assertEquals("pips: 1", newDie.toString());
	}

}
