package edu.westga.cs6910.pig.testing.die;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import java.util.Random;

import edu.westga.cs6910.pig.model.Die;

class TestWhenRoll {

	@Test
	public void testShouldFailIfPipIsGreaterthan6OrLessthan1() {
		Die testDie = new Die(new Random());
		for (int count = 0; count < 10000; count++) {
			testDie.roll();
			if (testDie.getNumberOfPips() < 1 || testDie.getNumberOfPips() > 6) {
				fail("Rolled a die with value: " + testDie.getNumberOfPips());
			}
		}
	}
}
