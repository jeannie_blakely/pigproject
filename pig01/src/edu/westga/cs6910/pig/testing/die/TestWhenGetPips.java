package edu.westga.cs6910.pig.testing.die;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import java.util.Random;

import edu.westga.cs6910.pig.model.Die;

class TestWhenGetPips {

	@Test
	public void testWithNewDieShouldHave1Pip() {
		Die newDie = new Die(new Random());
		assertEquals(1, newDie.getNumberOfPips());
	}

}
