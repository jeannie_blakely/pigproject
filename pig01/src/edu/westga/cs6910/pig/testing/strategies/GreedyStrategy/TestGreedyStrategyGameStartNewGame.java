package edu.westga.cs6910.pig.testing.strategies.GreedyStrategy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.Game;
import edu.westga.cs6910.pig.model.HumanPlayer;
import edu.westga.cs6910.pig.model.strategies.GreedyStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestGreedyStrategyGameStartNewGame {

	@Test
	public void testShouldProduceGameWithCurrentPlayerNameHuman() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newHumanPlayer);
		assertEquals("Human", newGame.getCurrentPlayer().getName());
	}
	
	@Test
	public void testShouldProduceGameWithCurrentPlayerNameSimpleComputer() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newComputerPlayer);
		assertEquals("Simple computer", newGame.getCurrentPlayer().getName());
	}
	
	@Test
	public void testShouldProduceGameWithCurrentPlayerNameHumanNewConstructor() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 32);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 32);
		newGame.startNewGame(newHumanPlayer);
		assertEquals("Human", newGame.getCurrentPlayer().getName());
	}
	
	@Test
	public void testShouldProduceGameWithCurrentPlayerNameSimpleComputerNewConstructor() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 66);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 66);
		newGame.startNewGame(newComputerPlayer);
		assertEquals("Simple computer", newGame.getCurrentPlayer().getName());
	}
}
