package edu.westga.cs6910.pig.testing.strategies.RandomStrategy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.strategies.RandomStrategy;

class TestRandomStrategyWhenGetName {

	@Test
	public void testShouldReturnRandom() {
		RandomStrategy newRandomStrategy = new RandomStrategy();
		assertEquals("Random", newRandomStrategy.getName());
	}
}
