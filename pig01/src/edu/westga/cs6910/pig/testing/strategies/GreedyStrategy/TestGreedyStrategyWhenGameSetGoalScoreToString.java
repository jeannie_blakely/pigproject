package edu.westga.cs6910.pig.testing.strategies.GreedyStrategy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.Game;
import edu.westga.cs6910.pig.model.HumanPlayer;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;
import edu.westga.cs6910.pig.model.strategies.GreedyStrategy;

class TestGreedyStrategyWhenGameSetGoalScoreToString {

	@Test
	public void testShouldProduceGameWithGoalScore50Human0SimpleComputer50GameOverWinnerComputer() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.setGoalScore(50);
		newGame.getComputerPlayer().setTotal(50);
		assertEquals("Goal Score: 50\r\n"
				+ "Human: 0\r\n"
				+ "Simple computer: 50\r\n"
				+ "Game over! Winner: Simple computer", newGame.toString());
	}
		
	@Test
	public void testShouldProduceGameWithGoalScore375Human375SimpleComputer0GameOverWinnerHuman() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.setGoalScore(375);
		newGame.getHumanPlayer().setTotal(375);
		assertEquals("Goal Score: 375\r\n"
				+ "Human: 375\r\n"
				+ "Simple computer: 0\r\n"
				+ "Game over! Winner: Human", newGame.toString());
	}
	
	/* 
	 * Tests defaults with bad input
	 */	
	@Test
	public void testShouldProduceGameWithGoalScore100Human100SimpleComputer0GameOverWinnerHuman() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.setGoalScore(0);
		newGame.getHumanPlayer().setTotal(100);
		assertEquals("Goal Score: 100\r\n"
				+ "Human: 100\r\n"
				+ "Simple computer: 0\r\n"
				+ "Game over! Winner: Human", newGame.toString());
	}
	
	/* 
	 * Tests defaults with bad input
	 */	
	@Test
	public void testShouldProduceGameWithGoalScore100Human0SimpleComputer100GameOverWinnerComputer() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.setGoalScore(-5);
		newGame.getComputerPlayer().setTotal(100);
		assertEquals("Goal Score: 100\r\n"
				+ "Human: 0\r\n"
				+ "Simple computer: 100\r\n"
				+ "Game over! Winner: Simple computer", newGame.toString());
	}

}
