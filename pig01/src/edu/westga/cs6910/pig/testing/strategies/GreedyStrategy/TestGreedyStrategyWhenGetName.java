package edu.westga.cs6910.pig.testing.strategies.GreedyStrategy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.strategies.GreedyStrategy;

class TestGreedyStrategyWhenGetName {

	@Test
	public void testShouldReturnGreedy() {
		GreedyStrategy newGreedyStrategy = new GreedyStrategy();
		assertEquals("Greedy", newGreedyStrategy.getName());
	}
}
