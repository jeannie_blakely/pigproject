package edu.westga.cs6910.pig.testing.strategies.RandomStrategy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.Game;
import edu.westga.cs6910.pig.model.HumanPlayer;
import edu.westga.cs6910.pig.model.strategies.RandomStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestRandomStrategyWhenGameGetCurrentPlayer {

	@Test
	public void testShouldProduceHumanPlayerWithNameHuman() {
		PigStrategy newPigStrategy = new RandomStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newHumanPlayer);
		assertEquals("Human", newGame.getCurrentPlayer().getName());
	}
	
	@Test
	public void testShouldProduceComputerPlayerWithNameSimplecomputer() {
		PigStrategy newPigStrategy = new RandomStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newComputerPlayer);
		assertEquals("Simple computer", newGame.getCurrentPlayer().getName());
	}
	
	@Test
	public void testShouldProduceHumanPlayerWithNameHumanNewConstructor() {
		PigStrategy newPigStrategy = new RandomStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 85);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 85);
		newGame.startNewGame(newHumanPlayer);
		assertEquals("Human", newGame.getCurrentPlayer().getName());
	}
	
	@Test
	public void testShouldProduceComputerPlayerWithNameSimplecomputerNewConstructor() {
		PigStrategy newPigStrategy = new RandomStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 60);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 60);
		newGame.startNewGame(newComputerPlayer);
		assertEquals("Simple computer", newGame.getCurrentPlayer().getName());
	}
}
