package edu.westga.cs6910.pig.testing.strategies.RandomStrategy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.Game;
import edu.westga.cs6910.pig.model.HumanPlayer;
import edu.westga.cs6910.pig.model.strategies.RandomStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestRandomStrategyWhenGameGetDicePair {

	@Test
	public void testShouldProduceGameWithDie1ValueOf1() {
		PigStrategy newPigStrategy = new RandomStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		assertEquals(1, newGame.getDicePair().getDie1Value());
	}
	
	@Test
	public void testShouldProduceGameWithDie2ValueOf1() {
		PigStrategy newPigStrategy = new RandomStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		assertEquals(1, newGame.getDicePair().getDie2Value());
	}
	
	@Test
	public void testShouldProduceGameWithDie1ValueOf1NewConstructor() {
		PigStrategy newPigStrategy = new RandomStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 75);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 75);
		assertEquals(1, newGame.getDicePair().getDie1Value());
	}
	
	@Test
	public void testShouldProduceGameWithDie2ValueOf1NewConstructor() {
		PigStrategy newPigStrategy = new RandomStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 75);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 75);
		assertEquals(1, newGame.getDicePair().getDie2Value());
	}

}
