package edu.westga.cs6910.pig.testing.strategies.GreedyStrategy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.Game;
import edu.westga.cs6910.pig.model.HumanPlayer;
import edu.westga.cs6910.pig.model.strategies.GreedyStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestGreedyStrategyWhenGameIsGameOver {

	/**
	 * No startGame method called to set CurrentPlayer which should
	 * be null.  Therefore isGameOver should be true    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverTrueBecauseCurrentPlayerNull() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		assertEquals(true, newGame.isGameOver());
	}
	
	/**
	 * startGame method called to set CurrentPlayer to
	 * HumanPlayer.  Therefore isGameOver should be false    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverFalseBecauseCurrentPlayerHumanPlayer() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newHumanPlayer);
		assertEquals(false, newGame.isGameOver());
	}
	
	/**
	 * startGame method called to set CurrentPlayer to
	 * ComputerPlayer.  Therefore isGameOver should be false    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverFalseBecauseCurrentPlayerComputerPlayer() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newComputerPlayer);
		assertEquals(false, newGame.isGameOver());
	}
	
	/**
	 * startGame method called to set CurrentPlayer to
	 * HumanPlayer.  Total for HumanPlayer set to 100
	 * Therefore isGameOver should be true    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverTrueBecauseCurrentPlayerHumanPlayerHasTotal100() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newHumanPlayer);
		newHumanPlayer.setTotal(100);
		assertEquals(true, newGame.isGameOver());
	}
	
	/**
	 * startGame method called to set CurrentPlayer to
	 * ComputerPlayer.  Total for ComputerPlayer set to 100
	 * Therefore isGameOver should be true    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverTrueBecauseComputerPlayerHasTotal100() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newComputerPlayer);
		newComputerPlayer.setTotal(100);
		assertEquals(true, newGame.isGameOver());
	}
	
	/**
	 * startGame method called to set CurrentPlayer to
	 * HumanPlayer.  Total for HumanPlayer set to 85
	 * Therefore isGameOver should be false    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverFalseBecauseCurrentPlayerHumanPlayerHasTotal85() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newHumanPlayer);
		newHumanPlayer.setTotal(85);
		assertEquals(false, newGame.isGameOver());
	}
	
	/**
	 * startGame method called to set CurrentPlayer to
	 * ComputerPlayer.  Total for ComputerPlayer set to 31
	 * Therefore isGameOver should be false    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverFalseBecauseCurrentPlayerComputerPlayerHasTotal31() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newComputerPlayer);
		newComputerPlayer.setTotal(31);
		assertEquals(false, newGame.isGameOver());
	}
	
	/**
	 * No startGame method called to set CurrentPlayer which should
	 * be null.  Therefore isGameOver should be true    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverTrueBecauseCurrentPlayerNullNewConstructor() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 75);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 75);
		assertEquals(true, newGame.isGameOver());
	}
	
	/**
	 * startGame method called to set CurrentPlayer to
	 * HumanPlayer.  Therefore isGameOver should be false    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverFalseBecauseCurrentPlayerHumanPlayerNewConstructor50() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 50);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 50);
		newGame.startNewGame(newHumanPlayer);
		assertEquals(false, newGame.isGameOver());
	}
	
	/**
	 * startGame method called to set CurrentPlayer to
	 * ComputerPlayer.  Therefore isGameOver should be false    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverFalseBecauseCurrentPlayerComputerPlayerNewConstructor25() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 25);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 25);
		newGame.startNewGame(newComputerPlayer);
		assertEquals(false, newGame.isGameOver());
	}
	
	/**
	 * startGame method called to set CurrentPlayer to
	 * HumanPlayer.  Total for HumanPlayer set to 100
	 * Therefore isGameOver should be true    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverTrueBecauseCurrentPlayerHumanPlayerHasTotal100NewConstructor97() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 97);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 97);
		newGame.startNewGame(newHumanPlayer);
		newHumanPlayer.setTotal(100);
		assertEquals(true, newGame.isGameOver());
	}
	
	/**
	 * startGame method called to set CurrentPlayer to
	 * ComputerPlayer.  Total for ComputerPlayer set to 100
	 * Therefore isGameOver should be true    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverFalseBecauseComputerPlayerHasTotal100NewConstructor125() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 125);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 125);
		newGame.startNewGame(newComputerPlayer);
		newComputerPlayer.setTotal(100);
		assertEquals(false, newGame.isGameOver());
	}
	
	/**
	 * startGame method called to set CurrentPlayer to
	 * HumanPlayer.  Total for HumanPlayer set to 85
	 * Goal is 75 in constructor
	 * Therefore isGameOver should be false    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverTrueBecauseCurrentPlayerHumanPlayerHasTotal85NewConstructor75() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 75);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 75);
		newGame.startNewGame(newHumanPlayer);
		newHumanPlayer.setTotal(85);
		assertEquals(true, newGame.isGameOver());
	}
	
	/**
	 * startGame method called to set CurrentPlayer to
	 * ComputerPlayer.  Total for ComputerPlayer set to 31
	 * Constructor sets goal for 50
	 * Therefore isGameOver should be false    
	 */
	@Test
	public void testShouldProduceGameWithIsGameOverFalseBecauseCurrentPlayerComputerPlayerHasTotal31NewConstructor50() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 50);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 50);
		newGame.startNewGame(newComputerPlayer);
		newComputerPlayer.setTotal(31);
		assertEquals(false, newGame.isGameOver());
	}
}
