package edu.westga.cs6910.pig.testing.strategies.CautiousStrategy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import edu.westga.cs6910.pig.model.strategies.CautiousStrategy;

import org.junit.jupiter.api.Test;

class TestCautiousStrategyWhenRollAgain {

	@Test
	public void testShouldReturnFalse() {
		CautiousStrategy newCautiousStrategy = new CautiousStrategy();
		assertEquals(false, newCautiousStrategy.rollAgain(1, 18, 63));
	}
	
	@Test
	public void testShouldReturnFalseAgain() {
		CautiousStrategy newCautiousStrategy = new CautiousStrategy();
		assertEquals(false, newCautiousStrategy.rollAgain(3, 0, 45));
	}

}
