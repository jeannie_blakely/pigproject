package edu.westga.cs6910.pig.testing.strategies.GreedyStrategy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import edu.westga.cs6910.pig.model.strategies.GreedyStrategy;

import org.junit.jupiter.api.Test;

class TestGreedyStrategyWhenRollAgain {

	@Test
	public void testRollsTakenIs1ShouldReturnTrue() {
		GreedyStrategy newGreedyStrategy = new GreedyStrategy();
		assertEquals(true, newGreedyStrategy.rollAgain(1, 15, 75));
	}
	
	@Test
	public void testRollsTakenIs2ShouldReturnTrue() {
		GreedyStrategy newGreedyStrategy = new GreedyStrategy();
		assertEquals(true, newGreedyStrategy.rollAgain(2, 15, 75));
	}
	
	@Test
	public void testRollsTakenIs3ShouldReturnTrue() {
		GreedyStrategy newGreedyStrategy = new GreedyStrategy();
		assertEquals(true, newGreedyStrategy.rollAgain(3, 15, 75));
	}
	
	@Test
	public void testRollsTakenIs4ShouldReturnFalse() {
		GreedyStrategy newGreedyStrategy = new GreedyStrategy();
		assertEquals(false, newGreedyStrategy.rollAgain(4, 15, 75));
	}
	
	@Test
	public void testRollsTakenIs28ShouldReturnFalse() {
		GreedyStrategy newGreedyStrategy = new GreedyStrategy();
		assertEquals(false, newGreedyStrategy.rollAgain(28, 15, 75));
	}
	
	@Test
	public void testHowCloseToGoalIs0ShouldReturnFalse() {
		GreedyStrategy newGreedyStrategy = new GreedyStrategy();
		assertEquals(true, newGreedyStrategy.rollAgain(1, 15, 75));
	}
	
	@Test
	public void testRHowCloseToGoalIsNeg1houldReturnFalse() {
		GreedyStrategy newGreedyStrategy = new GreedyStrategy();
		assertEquals(false, newGreedyStrategy.rollAgain(2, 15, -1));
	}
	
	@Test
	public void testHowCloseToGoalIsNeg5ShouldReturnFalse() {
		GreedyStrategy newGreedyStrategy = new GreedyStrategy();
		assertEquals(false, newGreedyStrategy.rollAgain(3, 15, -5));
	}
	
	@Test
	public void testHowCloseToGoalIs1ShouldReturnTrue() {
		GreedyStrategy newGreedyStrategy = new GreedyStrategy();
		assertEquals(true, newGreedyStrategy.rollAgain(2, 15, 1));
	}
	
	@Test
	public void testHowCloseToGoalIsNeg11ShouldReturnFalse() {
		GreedyStrategy newGreedyStrategy = new GreedyStrategy();
		assertEquals(false, newGreedyStrategy.rollAgain(28, 15, -11));
	}

}
