package edu.westga.cs6910.pig.testing.strategies.GreedyStrategy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.strategies.GreedyStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestGreedyStrategyWhenComputerPlayerGetIsMyTurn {

	@Test
	public void testShouldProduceComputerPlayerWithIsMyTurnFalse() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		assertEquals(false, newComputerPlayer.getIsMyTurn());
	}
	
	@Test
	public void testShouldProduceComputerPlayerWithIsMyTurnFalseNewConstructor() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 25);
		assertEquals(false, newComputerPlayer.getIsMyTurn());
	}
}
