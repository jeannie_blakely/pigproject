package edu.westga.cs6910.pig.testing.strategies.GreedyStrategy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.Game;
import edu.westga.cs6910.pig.model.HumanPlayer;
import edu.westga.cs6910.pig.model.strategies.GreedyStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestGreedyStrategyGameHoldSwapWhoseTurn {
	@Test
	public void testShouldProduceGameWithCurrentPlayerComputerBecauseHoldSwitchedPlayers() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newHumanPlayer);
		newGame.hold();
		assertEquals("Simple computer", newGame.getCurrentPlayer().getName());
	}
	
	@Test
	public void testShouldProduceGameWithCurrentPlayerHumanBecauseHoldSwitchedPlayers() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newComputerPlayer);
		newGame.hold();
		assertEquals("Human", newGame.getCurrentPlayer().getName());
	}
	
	@Test
	public void testShouldProduceGameWithCurrentPlayerComputerBecauseHoldSwitchedPlayersNewConstructor() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 50);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 50);
		newGame.startNewGame(newHumanPlayer);
		newGame.hold();
		assertEquals("Simple computer", newGame.getCurrentPlayer().getName());
	}
	
	@Test
	public void testShouldProduceGameWithCurrentPlayerHumanBecauseHoldSwitchedPlayersNewConstructor() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 75);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 75);
		newGame.startNewGame(newComputerPlayer);
		newGame.hold();
		assertEquals("Human", newGame.getCurrentPlayer().getName());
	}
}
