package edu.westga.cs6910.pig.testing.strategies.GreedyStrategy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.Game;
import edu.westga.cs6910.pig.model.HumanPlayer;
import edu.westga.cs6910.pig.model.strategies.GreedyStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestGreedyStrategyWhenGameToString {

	@Test
	public void testShouldProduceGameWithGoalScore100Human100SimpleComputer0GameOverWinnerHuman() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.getHumanPlayer().setTotal(100);
		assertEquals("Goal Score: 100\r\n"
				+ "Human: 100\r\n"
				+ "Simple computer: 0\r\n"
				+ "Game over! Winner: Human", newGame.toString());
	}
	
	@Test
	public void testShouldProduceGameWithGoalScore100Human0SimpleComputer100GameOverWinnerComputer() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.getComputerPlayer().setTotal(100);
		assertEquals("Goal Score: 100\r\n"
				+ "Human: 0\r\n"
				+ "Simple computer: 100\r\n"
				+ "Game over! Winner: Simple computer", newGame.toString());
	}
	
	@Test
	public void testShouldProduceGameWithGoalScore50Human50SimpleComputer0GameOverWinnerHumanNewConstructor() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 50);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 50);
		newGame.getHumanPlayer().setTotal(50);
		assertEquals("Goal Score: 50\r\n"
				+ "Human: 50\r\n"
				+ "Simple computer: 0\r\n"
				+ "Game over! Winner: Human", newGame.toString());
	}
	
	@Test
	public void testShouldProduceGameWithGoalScore175Human0SimpleComputer175GameOverWinnerComputerNewConstructor() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 175);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 175);
		newGame.getComputerPlayer().setTotal(175);
		assertEquals("Goal Score: 175\r\n"
				+ "Human: 0\r\n"
				+ "Simple computer: 175\r\n"
				+ "Game over! Winner: Simple computer", newGame.toString());
	}
	
	@Test
	public void testShouldProduceGameWithGoalScore175Human0SimpleComputer175GameOverWinnerComputerNewConstructorInvalid() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, -10);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, -10);
		newGame.getComputerPlayer().setTotal(100);
		assertEquals("Goal Score: 100\r\n"
				+ "Human: 0\r\n"
				+ "Simple computer: 100\r\n"
				+ "Game over! Winner: Simple computer", newGame.toString());
	}
}
