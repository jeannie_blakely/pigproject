package edu.westga.cs6910.pig.testing.strategies.GreedyStrategy;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.Game;
import edu.westga.cs6910.pig.model.HumanPlayer;
import edu.westga.cs6910.pig.model.strategies.GreedyStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestGreedyStrategyWhenGamePlay {
	@Test
	public void testShouldProduceFailIfTotalAndIsGameOverDoNotAlignWithOver100() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		//newComputerPlayer.setMaximumRolls();
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newHumanPlayer);
		for (int count = 0; count < 10000; count++) {
			newGame.play();
			if ((newGame.getComputerPlayer().getTotal() >= 100 || newGame.getHumanPlayer().getTotal() >= 100) && !newGame.isGameOver()) {
				fail(newGame.getCurrentPlayer().getName() + " with total of " + newGame.getCurrentPlayer().getTotal() + " and isGameOver is " + newGame.isGameOver());
			}
		}
	}
	
	@Test
	public void testShouldProduceFailIfTotalAndIsGameOverDoNotAlignWithOver125NewConstructor() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 125);
		//newComputerPlayer.setMaximumRolls();
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 125);
		newGame.startNewGame(newHumanPlayer);
		for (int count = 0; count < 10000; count++) {
			newGame.play();
			if ((newGame.getComputerPlayer().getTotal() >= 125 || newGame.getHumanPlayer().getTotal() >= 125) && !newGame.isGameOver()) {
				fail(newGame.getCurrentPlayer().getName() + " with total of " + newGame.getCurrentPlayer().getTotal() + " and isGameOver is " + newGame.isGameOver());
			}
		}
	}
	
	@Test
	public void testShouldProduceFailIfTotalAndIsGameOverDoNotAlignWithOver350NewConstructor() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 350);
		//newComputerPlayer.setMaximumRolls();
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 350);
		newGame.startNewGame(newHumanPlayer);
		for (int count = 0; count < 10000; count++) {
			newGame.play();
			if ((newGame.getComputerPlayer().getTotal() >= 350 || newGame.getHumanPlayer().getTotal() >= 350) && !newGame.isGameOver()) {
				fail(newGame.getCurrentPlayer().getName() + " with total of " + newGame.getCurrentPlayer().getTotal() + " and isGameOver is " + newGame.isGameOver());
			}
		}
	}
	
	@Test
	public void testShouldProduceFailIfTotalAndIsGameOverDoNotAlignWithOver100NewConstructorInvalidData() {
		PigStrategy newPigStrategy = new GreedyStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 0);
		//newComputerPlayer.setMaximumRolls();
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 0);
		newGame.startNewGame(newHumanPlayer);
		for (int count = 0; count < 10000; count++) {
			newGame.play();
			if ((newGame.getComputerPlayer().getTotal() >= 100 || newGame.getHumanPlayer().getTotal() >= 100) && !newGame.isGameOver()) {
				fail(newGame.getCurrentPlayer().getName() + " with total of " + newGame.getCurrentPlayer().getTotal() + " and isGameOver is " + newGame.isGameOver());
			}
		}
	}
}
