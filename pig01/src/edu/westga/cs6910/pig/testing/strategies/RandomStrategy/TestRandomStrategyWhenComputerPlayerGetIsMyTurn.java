package edu.westga.cs6910.pig.testing.strategies.RandomStrategy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.strategies.RandomStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestRandomStrategyWhenComputerPlayerGetIsMyTurn {

	@Test
	public void testShouldProduceComputerPlayerWithIsMyTurnFalse() {
		PigStrategy newPigStrategy = new RandomStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		assertEquals(false, newComputerPlayer.getIsMyTurn());
	}
	
	@Test
	public void testShouldProduceComputerPlayerWithIsMyTurnFalseNewConstructor() {
		PigStrategy newPigStrategy = new RandomStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 125);
		assertEquals(false, newComputerPlayer.getIsMyTurn());
	}

}
