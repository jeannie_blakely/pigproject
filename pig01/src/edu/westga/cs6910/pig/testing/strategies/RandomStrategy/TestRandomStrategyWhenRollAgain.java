package edu.westga.cs6910.pig.testing.strategies.RandomStrategy;

import static org.junit.jupiter.api.Assertions.fail;

import edu.westga.cs6910.pig.model.strategies.RandomStrategy;

import org.junit.jupiter.api.Test;

class TestRandomStrategyWhenRollAgain {

	@Test
	public void testShouldFailIfTallyOfFalseReturnsIsLessThan100() {
		RandomStrategy newRandomStrategy = new RandomStrategy();
		int falseCount = 0;
		for (int count = 0; count < 10000; count++) {
			if (!newRandomStrategy.rollAgain(0, 10, 45)) {
				falseCount++;
			}
		}
		if (falseCount <= 100) {
			fail("Less than 100 false returns counted");
		}
	}
	
	@Test
	public void testShouldFailIfTallyOfTrueReturnsIsLessThan100() {
		RandomStrategy newRandomStrategy = new RandomStrategy();
		int trueCount = 0;
		for (int count = 0; count < 10000; count++) {
			if (!newRandomStrategy.rollAgain(1, 5, 18)) {
				trueCount++;
			}
		}
		if (trueCount <= 100) {
			fail("Less than 100 true returns counted");
		}
	}
	
	@Test
	public void testShouldFailIfNotAllFalseWhenHowCloseToGoalIs0() {
		RandomStrategy newRandomStrategy = new RandomStrategy();
		int falseCount = 0;
		for (int count = 0; count < 10000; count++) {
			if (!newRandomStrategy.rollAgain(0, 10, 0)) {
				falseCount++;
			}
		}
		if (falseCount != 10000) {
			fail("Random Strategy is not always false");
		}
	}
	
	@Test
	public void testShouldFailIfNotAllFalseWhenHowCloseToGoalIsNeg1() {
		RandomStrategy newRandomStrategy = new RandomStrategy();
		int falseCount = 0;
		for (int count = 0; count < 10000; count++) {
			if (!newRandomStrategy.rollAgain(1, 5, -1)) {
				falseCount++;
			}
		}
		if (falseCount != 10000) {
			fail("Random Strategy is not always false");
		}
	}

}
