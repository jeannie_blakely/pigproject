package edu.westga.cs6910.pig.testing.strategies.RandomStrategy;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.Game;
import edu.westga.cs6910.pig.model.HumanPlayer;
import edu.westga.cs6910.pig.model.strategies.RandomStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestRandomStrategyWhenGameHoldResetTurnTotal {

	@Test
	public void testShouldPassIfAllComputerPlayerObjectAreResetToTurnTotal0() {
		PigStrategy newPigStrategy = new RandomStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		//newComputerPlayer.setMaximumRolls();
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newComputerPlayer);
		for (int count = 0; count < 10000; count++) {
			newGame.play();
			newGame.hold();
			if (newGame.getComputerPlayer().getTurnTotal() != 0) {
				fail(newGame.getComputerPlayer().getName() + " with turnTotal of " + newGame.getComputerPlayer().getTurnTotal());
			}
		}
	}
	
	@Test
	public void testShouldPassIfAllHumanPlayerObjectsAreResetToTurnTotal0() {
		PigStrategy newPigStrategy = new RandomStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		//newComputerPlayer.setMaximumRolls();
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newComputerPlayer);
		for (int count = 0; count < 10000; count++) {
			newGame.play();
			newGame.hold();
			if (newGame.getHumanPlayer().getTurnTotal() != 0) {
				fail(newGame.getHumanPlayer().getName() + " with turnTotal of " + newGame.getHumanPlayer().getTurnTotal());
			}
		}
	}

	@Test
	public void testShouldPassIfAllComputerPlayerObjectAreResetToTurnTotal0NewConsructor() {
		PigStrategy newPigStrategy = new RandomStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 50);
		//newComputerPlayer.setMaximumRolls();
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 50);
		newGame.startNewGame(newComputerPlayer);
		for (int count = 0; count < 10000; count++) {
			newGame.play();
			newGame.hold();
			if (newGame.getComputerPlayer().getTurnTotal() != 0) {
				fail(newGame.getComputerPlayer().getName() + " with turnTotal of " + newGame.getComputerPlayer().getTurnTotal());
			}
		}
	}

}
