package edu.westga.cs6910.pig.testing.strategies.RandomStrategy;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.Game;
import edu.westga.cs6910.pig.model.HumanPlayer;
import edu.westga.cs6910.pig.model.strategies.RandomStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestRandomStrategyWhenGamePlay {
	@Test
	public void testShouldProduceFailIfTotalAndIsGameOverDoNotAlignWithOver100() {
		PigStrategy newPigStrategy = new RandomStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		//newComputerPlayer.setMaximumRolls();
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.startNewGame(newHumanPlayer);
		for (int count = 0; count < 10000; count++) {
			newGame.play();
			if ((newGame.getComputerPlayer().getTotal() >= 100 || newGame.getHumanPlayer().getTotal() >= 100) && !newGame.isGameOver()) {
				fail(newGame.getCurrentPlayer().getName() + " with total of " + newGame.getCurrentPlayer().getTotal() + " and isGameOver is " + newGame.isGameOver());
			}
		}
	}
	
	@Test
	public void testShouldProduceFailIfTotalAndIsGameOverDoNotAlignWithOver175NewConstructor() {
		PigStrategy newPigStrategy = new RandomStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 175);
		//newComputerPlayer.setMaximumRolls();
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 175);
		newGame.startNewGame(newHumanPlayer);
		for (int count = 0; count < 10000; count++) {
			newGame.play();
			if ((newGame.getComputerPlayer().getTotal() >= 175 || newGame.getHumanPlayer().getTotal() >= 175) && !newGame.isGameOver()) {
				fail(newGame.getCurrentPlayer().getName() + " with total of " + newGame.getCurrentPlayer().getTotal() + " and isGameOver is " + newGame.isGameOver());
			}
		}
	}
	
	@Test
	public void testShouldProduceFailIfTotalAndIsGameOverDoNotAlignWithOver31NewConstructor() {
		PigStrategy newPigStrategy = new RandomStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 31);
		//newComputerPlayer.setMaximumRolls();
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 31);
		newGame.startNewGame(newHumanPlayer);
		for (int count = 0; count < 10000; count++) {
			newGame.play();
			if ((newGame.getComputerPlayer().getTotal() >= 31 || newGame.getHumanPlayer().getTotal() >= 31) && !newGame.isGameOver()) {
				fail(newGame.getCurrentPlayer().getName() + " with total of " + newGame.getCurrentPlayer().getTotal() + " and isGameOver is " + newGame.isGameOver());
			}
		}
	}
	
	@Test
	public void testShouldProduceFailIfTotalAndIsGameOverDoNotAlignWithOver100NewConstructorWithInvalidParam() {
		PigStrategy newPigStrategy = new RandomStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 0);
		//newComputerPlayer.setMaximumRolls();
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 0);
		newGame.startNewGame(newHumanPlayer);
		for (int count = 0; count < 10000; count++) {
			newGame.play();
			if ((newGame.getComputerPlayer().getTotal() >= 100 || newGame.getHumanPlayer().getTotal() >= 100) && !newGame.isGameOver()) {
				fail(newGame.getCurrentPlayer().getName() + " with total of " + newGame.getCurrentPlayer().getTotal() + " and isGameOver is " + newGame.isGameOver());
			}
		}
	}
}
