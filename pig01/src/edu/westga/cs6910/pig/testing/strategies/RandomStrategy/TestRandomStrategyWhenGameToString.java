package edu.westga.cs6910.pig.testing.strategies.RandomStrategy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.Game;
import edu.westga.cs6910.pig.model.HumanPlayer;
import edu.westga.cs6910.pig.model.strategies.RandomStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestRandomStrategyWhenGameToString {

	@Test
	public void testShouldProduceGameWithGoalScore100Human100SimpleComputer0GameOverWinnerHuman() {
		PigStrategy newPigStrategy = new RandomStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.getHumanPlayer().setTotal(100);
		assertEquals("Goal Score: 100\r\n"
				+ "Human: 100\r\n"
				+ "Simple computer: 0\r\n"
				+ "Game over! Winner: Human", newGame.toString());
	}
	
	@Test
	public void testShouldProduceGameWithGoalScore100Human0SimpleComputer100GameOverWinnerComputer() {
		PigStrategy newPigStrategy = new RandomStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.getComputerPlayer().setTotal(100);
		assertEquals("Goal Score: 100\r\n"
				+ "Human: 0\r\n"
				+ "Simple computer: 100\r\n"
				+ "Game over! Winner: Simple computer", newGame.toString());
	}
	
	/* 
	 * Added to check the setGoalScore
	 */	
	@Test
	public void testShouldProduceGameWithGoalScore50Human0SimpleComputer50GameOverWinnerComputer() {
		PigStrategy newPigStrategy = new RandomStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.setGoalScore(50);
		newGame.getComputerPlayer().setTotal(50);
		assertEquals("Goal Score: 50\r\n"
				+ "Human: 0\r\n"
				+ "Simple computer: 50\r\n"
				+ "Game over! Winner: Simple computer", newGame.toString());
	}
	
	/* 
	 * Added to check the setGoalScore
	 */	
	@Test
	public void testShouldProduceGameWithGoalScore375Human375SimpleComputer0GameOverWinnerHuman() {
		PigStrategy newPigStrategy = new RandomStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer);
		newGame.setGoalScore(375);
		newGame.getHumanPlayer().setTotal(375);
		assertEquals("Goal Score: 375\r\n"
				+ "Human: 375\r\n"
				+ "Simple computer: 0\r\n"
				+ "Game over! Winner: Human", newGame.toString());
	}
	
	/* 
	 * Added to check the new constructor
	 */	
	@Test
	public void testShouldProduceGameWithGoalScore375Human375SimpleComputer0GameOverWinnerHumanNewConstructor() {
		PigStrategy newPigStrategy = new RandomStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 375);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 375);
		newGame.getHumanPlayer().setTotal(375);
		assertEquals("Goal Score: 375\r\n"
				+ "Human: 375\r\n"
				+ "Simple computer: 0\r\n"
				+ "Game over! Winner: Human", newGame.toString());
	}
	
	/* 
	 * Added to check the new constructor
	 */	
	@Test
	public void testShouldProduceGameWithGoalScore25Human25SimpleComputer0GameOverWinnerHumanNewConstructor() {
		PigStrategy newPigStrategy = new RandomStrategy();
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 25);
		Game newGame = new Game(newHumanPlayer, newComputerPlayer, 25);
		newGame.getHumanPlayer().setTotal(25);
		assertEquals("Goal Score: 25\r\n"
				+ "Human: 25\r\n"
				+ "Simple computer: 0\r\n"
				+ "Game over! Winner: Human", newGame.toString());
	}
}
