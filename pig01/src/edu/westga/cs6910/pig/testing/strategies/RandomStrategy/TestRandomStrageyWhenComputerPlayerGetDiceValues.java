package edu.westga.cs6910.pig.testing.strategies.RandomStrategy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.ComputerPlayer;
import edu.westga.cs6910.pig.model.strategies.RandomStrategy;
import edu.westga.cs6910.pig.model.strategies.PigStrategy;

class TestRandomStrageyWhenComputerPlayerGetDiceValues {

	@Test
	public void testShouldProduceComputerPlayerWithDiceValues1Comma1() {
		PigStrategy newPigStrategy = new RandomStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy);
		assertEquals("1, 1", newComputerPlayer.getDiceValues());
	}
	
	@Test
	public void testShouldProduceComputerPlayerWithDiceValues1Comma1NewConstructor() {
		PigStrategy newPigStrategy = new RandomStrategy();
		ComputerPlayer newComputerPlayer = new ComputerPlayer(newPigStrategy, 350);
		assertEquals("1, 1", newComputerPlayer.getDiceValues());
	}
}
