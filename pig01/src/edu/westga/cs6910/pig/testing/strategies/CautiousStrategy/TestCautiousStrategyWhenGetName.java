package edu.westga.cs6910.pig.testing.strategies.CautiousStrategy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.strategies.CautiousStrategy;

class TestCautiousStrategyWhenGetName {

	@Test
	public void testShouldReturnCautious() {
		CautiousStrategy newCautiousStrategy = new CautiousStrategy();
		assertEquals("Cautious", newCautiousStrategy.getName());
	}

}
