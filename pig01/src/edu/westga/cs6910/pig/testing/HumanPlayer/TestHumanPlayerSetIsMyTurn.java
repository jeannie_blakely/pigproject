package edu.westga.cs6910.pig.testing.HumanPlayer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.HumanPlayer;

class TestHumanPlayerSetIsMyTurn {

	@Test
	public void testSetIsMyTurnShouldBeTrue() {
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		newHumanPlayer.setIsMyTurn(true);
		assertEquals(true, newHumanPlayer.getIsMyTurn());
	}
	
	@Test
	public void testSetIsMyTurnShouldBeFalse() {
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		newHumanPlayer.setIsMyTurn(false);
		assertEquals(false, newHumanPlayer.getIsMyTurn());
	}
}
