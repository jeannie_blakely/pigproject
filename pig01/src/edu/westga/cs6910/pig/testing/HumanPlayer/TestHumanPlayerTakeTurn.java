package edu.westga.cs6910.pig.testing.HumanPlayer;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.HumanPlayer;

class TestHumanPlayerTakeTurn {

	@Test
	public void testShouldFailIfIsMyTurnIsTrueWhenEitherDicePairIs1() {
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		for (int count = 0; count < 10000; count++) {
			newHumanPlayer.takeTurn();
			if ((newHumanPlayer.getDiceValues().startsWith("1") || newHumanPlayer.getDiceValues().endsWith("1")) && newHumanPlayer.getIsMyTurn()) {
				fail("Dice values are: " + newHumanPlayer.getDiceValues() + " and isMyTurn is : " + newHumanPlayer.getIsMyTurn());
			}
		}
	}
}
