package edu.westga.cs6910.pig.testing.HumanPlayer;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.HumanPlayer;

class TestHumanPlayerResetTurnTotal {

	@Test
	public void testShouldFailIfResetTurnTotalDoesNotSetTurnTotalValueTo0() {
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		for (int count = 0; count < 10000; count++) {
			newHumanPlayer.takeTurn();
			newHumanPlayer.resetTurnTotal();
			if (newHumanPlayer.getTurnTotal() != 0) {
				fail("Human Player with turnTotal of " + newHumanPlayer.getTurnTotal() + " after resetTurnTotal");
			}
		}
	}
}
