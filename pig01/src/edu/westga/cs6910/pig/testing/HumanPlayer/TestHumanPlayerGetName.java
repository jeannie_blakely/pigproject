package edu.westga.cs6910.pig.testing.HumanPlayer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.HumanPlayer;

class TestHumanPlayerGetName {

	@Test
	public void testShouldProduceHumanPlayerWithStringNameHuman() {
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		assertEquals("Human", newHumanPlayer.getName());
	}
	
	@Test
	public void testShouldProduceHumanPlayerWithStringNameAlfred() {
		HumanPlayer newHumanPlayer = new HumanPlayer("Alfred");
		assertEquals("Alfred", newHumanPlayer.getName());
	}

}
