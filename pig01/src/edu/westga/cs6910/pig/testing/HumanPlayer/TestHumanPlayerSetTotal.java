package edu.westga.cs6910.pig.testing.HumanPlayer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.HumanPlayer;

class TestHumanPlayerSetTotal {

	@Test
	public void testShouldProduceHumanPlayerWithTotal0() {
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		newHumanPlayer.setTotal(0);
		assertEquals(0, newHumanPlayer.getTotal());
	}
	
	@Test
	public void testShouldProduceHumanPlayerWithTotal89() {
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		newHumanPlayer.setTotal(89);
		assertEquals(89, newHumanPlayer.getTotal());
	}
	
	@Test
	public void testShouldProduceHumanPlayerWithTotal100() {
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		newHumanPlayer.setTotal(100);
		assertEquals(100, newHumanPlayer.getTotal());
	}
}
