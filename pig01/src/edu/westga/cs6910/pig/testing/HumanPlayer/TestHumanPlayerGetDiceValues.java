package edu.westga.cs6910.pig.testing.HumanPlayer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.HumanPlayer;

class TestHumanPlayerGetDiceValues {

	@Test
	public void testShouldProduceHumanPlayerWithDiceValues1Comma1() {
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		assertEquals("1, 1", newHumanPlayer.getDiceValues());
	}
}
