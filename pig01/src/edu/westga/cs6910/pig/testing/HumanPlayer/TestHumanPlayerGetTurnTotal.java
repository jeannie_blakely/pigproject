package edu.westga.cs6910.pig.testing.HumanPlayer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.HumanPlayer;

class TestHumanPlayerGetTurnTotal {

	@Test
	public void testShouldProduceHumanPlayerWithTurnTotal0() {
		HumanPlayer newHumanPlayer = new HumanPlayer("Human");
		assertEquals(0, newHumanPlayer.getTurnTotal());
	}
}
