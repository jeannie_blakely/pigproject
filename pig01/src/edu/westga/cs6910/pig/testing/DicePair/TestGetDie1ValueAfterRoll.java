package edu.westga.cs6910.pig.testing.DicePair;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.DicePair;

class TestGetDie1ValueAfterRoll {

	@Test
	public void testShouldFailIfGetDie1VauleIsGreaterthan6OrLessthan1() {
		DicePair testDicePair = new DicePair();
		for (int count = 0; count < 10000; count++) {
			testDicePair.rollDice();
			if (testDicePair.getDie1Value() < 1 || testDicePair.getDie1Value() > 6) {
				fail("Rolled a DicePair with value of Die 1: " + testDicePair.getDie1Value());
			}
		}
	}
}
