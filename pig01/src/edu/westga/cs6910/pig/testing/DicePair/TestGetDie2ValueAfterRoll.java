package edu.westga.cs6910.pig.testing.DicePair;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.DicePair;

class TestGetDie2ValueAfterRoll {

	@Test
	public void testShouldFailIfGetDie2VauleIsGreaterthan6OrLessthan1() {
		DicePair testDicePair = new DicePair();
		for (int count = 0; count < 10000; count++) {
			testDicePair.rollDice();
			if (testDicePair.getDie2Value() < 1 || testDicePair.getDie2Value() > 6) {
				fail("Rolled a DicePair with value of Die 2: " + testDicePair.getDie2Value());
			}
		}
	}
}
