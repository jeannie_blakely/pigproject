package edu.westga.cs6910.pig.testing.DicePair;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs6910.pig.model.DicePair;

class TestGetDie1ValueWhenDicePairIsCreated {

	@Test
	public void testWhenDicePairIsCreatedDice1ShouldBe1() {
		DicePair newDicePair = new DicePair();
		assertEquals(1, newDicePair.getDie1Value());
	}
}
