package edu.westga.cs6910.pig.model;

import edu.westga.cs6910.pig.model.strategies.PigStrategy;

// Classes ComputerPlayer and HumanPlayer share most of their code.
//		 Refactor their code:
// 		 1. Create abstract base class AbstractPlayer to implement the
//			shared code and define abstract methods for methods without
//			duplicate code. AbstractPlayer should implement interface Player.
//		 2. Have ComputerPlayer and HumanPlayer extend AbstractPlayer to
//		    implement the unshared constructor code and the abstract methods.

/**
 * ComputerPlayer represents a very simple automated player in the game Pig.
 * It rolls exactly 1 time and then holds.
 * 
 * @author	CS6910
 * @version	Summer 2018
 * 
 * @author Jeannie Blakely
 * @version 6-2-18
 */
public class ComputerPlayer extends AbstractPlayer {
	private static final String NAME = "Simple computer";
	//Commented this out after implementing strategies with rollsTaken as input <jbb 6-19>
	//private int maximumRolls;
	private PigStrategy thePigStrategy;
	private int goalScore;
	
		
	/**
	 * Creates a new ComputerPlayer with the specified name and a user defined goalScore
	 * @param	newPigStrategy 
	 * @precondition	newPigStrategy != null
	 */
	public ComputerPlayer(PigStrategy newPigStrategy) {
		this(newPigStrategy, 100);
	}
	
	/**
	 * Creates a new ComputerPlayer with the specified name and a user defined goalScore
	 * @param	newPigStrategy 
	 * @param	theGoalScore input goalScore
	 * @precondition	newPigStrategy != null
	 * @precondition	theGoalScore > 0
	 */
	public ComputerPlayer(PigStrategy newPigStrategy, int theGoalScore) {
		super(NAME);
		super.setTotal(0);
		if (newPigStrategy == null) {
			throw new IllegalArgumentException("Invalid strategy; ComputerPlayer not created");
		}
		this.thePigStrategy = newPigStrategy;
		if (theGoalScore <= 0) {
			this.goalScore = 100;
		} else {
			this.goalScore = theGoalScore;
		}
	}

	//*************************** mutator methods ****************************
	
	/**
	 * Implements Player's setMaximumRolls, but is not normally
	 * called by client objects.  Instead, clients usually
	 * call the 0-parameter version
	 * 
	 * @param	maximumRolls	The maximum number of times the computer
	 * 							will roll before holding
	 * 
	 * Commented this out after implementing strategies with rollsTaken as input <jbb 6-19>
	 */
	//public void setMaximumRolls(int maximumRolls) {
	//	this.maximumRolls = maximumRolls;
	//}

	/**
	 * Implements Player's setMaximumRolls() to set the 
	 * maximum number of rolls to 1
	 * 
	 * Commented this out after implementing strategies with rollsTaken as input <jbb 6-19>
	 * 
	 */
	//public void setMaximumRolls() {
	//	this.maximumRolls = 1;
	//}
	

	/**
	 * Sets the PigStrategy to another strategy; will determine how ComputerPlayer
	 * will play
	 * @param 	newPigStrategy  the PigStrategy to be used
	 * @precondition	newPigStrategy != null
	 */
	public void setStrategy(PigStrategy newPigStrategy) {
		if (newPigStrategy == null) {
			throw new IllegalArgumentException("Invalid argument; strategy not set");
		}
		this.thePigStrategy = newPigStrategy; 
	}
	
	/**
	 * Returns the current PigStrategy
	 * @return 	thePigStrategy
	 */
	public PigStrategy getStrategy() {
		return this.thePigStrategy; 
	}

	@Override
	/**
	 * @see Player#takeTurn()
	 * References the PigStrategy interface to decide whether or not to roll again
	 * Breaks out of loop if 1 is rolled
	 */	
	public void takeTurn() {
		int rollsTaken = 1;
		do {
			super.takeTurn();
			rollsTaken++;
			if (!super.getIsMyTurn()) {
				break;
			}
		} while (this.thePigStrategy.rollAgain(rollsTaken, super.getTurnTotal(), (this.goalScore - super.getTotal())));
		super.setIsMyTurn(false);
	}
}
