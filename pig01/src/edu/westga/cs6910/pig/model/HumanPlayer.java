package edu.westga.cs6910.pig.model;

/**
 * HumanPlayer represents a human player in the game Pig.
 * 
 * @author	CS6910
 * @version	Summer 2018
 * 
 * @author Jeannie Blakely
 * @version 6-2-18
 */
public class HumanPlayer extends AbstractPlayer {
	
	/**
	 * Creates a new HumanPlayer with the specified name.
	 * 
	 * @param name	this Player's name
	 * 
	 * @requires	name != null
	 * @ensure		name().equals(name) && getTotal() == 0
	 */
	public HumanPlayer(String name) {
		super(name);
	}

}
