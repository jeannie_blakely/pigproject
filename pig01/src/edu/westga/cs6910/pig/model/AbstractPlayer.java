package edu.westga.cs6910.pig.model;

/**
 * An abstract class for AbstractPlayer objects.     
 * 
 * @author Jeannie Blakely
 * @version 6-2-18
 *
 */

public abstract class AbstractPlayer implements Player {
	private String name;
	private DicePair thePair;
	private boolean isMyTurn;
	private int turnTotal;
	private int total;

	/**
	 * Creates a new AbstractPlayer object and initializes the name and thePair (DicePair)
	 * 
	 * @param name		The player name
	 * @precondition 	name != null
	 */
	public AbstractPlayer(String name) {
		this.name = name;
		this.thePair = new DicePair();
	}
	

	// ************************** mutator methods ********************************	
	@Override
	/**
	 * @see Player#resetTurnTotal()
	 */	
	public void resetTurnTotal() {
		this.turnTotal = 0;
	}

	@Override
	/**
	 * @see Player#takeTurn()
	 */
	public void takeTurn() {	
		this.thePair.rollDice();
		
		int die1Value = this.thePair.getDie1Value();
		int die2Value = this.thePair.getDie2Value();
		if (die1Value == 1 || die2Value == 1) {	
			this.total -= this.turnTotal;
			this.isMyTurn = false;
		} else {
			this.turnTotal += die1Value + die2Value;
			this.total += die1Value + die2Value;
			this.isMyTurn = true;
		}
	}
	
	/**
	 * Setter for isMyTurn
	 * @param 	value boolean value
	 */
	public void setIsMyTurn(boolean value) {
		this.isMyTurn = value;
	}
	
	/**
	 * Setter for total
	 * @param	total int total
	 */
	public void setTotal(int total) {
		this.total = total;
	}
	
	//*************************** accessor methods ****************************
	@Override	
	/**
	 * @see Player#getIsMyTurn()
	 */
	public boolean getIsMyTurn() {
		return this.isMyTurn;
	}
		
	@Override	
	/**
	 * @see Player#getTurnTotal()
	*/
	public int getTurnTotal() {
		return this.turnTotal;
	}
		
	@Override
	/**
	 * @see Player#getName()
	 */
	public String getName() {
		return this.name;
	}
		
	@Override
	/**
	 * @see Player#getDiceValues()
	 */
	public String getDiceValues() {
		return this.thePair.getDie1Value() + ", " + this.thePair.getDie2Value();
	}
		
	@Override
	/**
	 * @see Player#getTotal()
	 */
	public int getTotal() {
		return this.total;
	}
}
