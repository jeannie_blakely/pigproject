package edu.westga.cs6910.pig.model.strategies;

/**
 * Implements the game-play strategy that always returns false (ie never 
 * rolls again)
 * 
 * @author Jeannie Blakely
 * @version 6-14-18
 */
public class CautiousStrategy implements PigStrategy {
	
	/**
	 * Implements PigStrategy's rollAgain() to return false (ie
	 * just roll once)
	 * 
	 */
	@Override
	public boolean rollAgain(int rollsTaken, int turnTotal, int howCloseToGoal) {
		return false;
	}
	
	/**
	 * 	Returns the strategy name
	 * @return	String
	 */
	@Override
	public String getName() {
		return "Cautious";
	}
}
