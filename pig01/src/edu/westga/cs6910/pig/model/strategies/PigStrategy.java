package edu.westga.cs6910.pig.model.strategies;

/**
 * Defines the common interface for all the game=play algorithms for Pig
 * 
 * @author Jeannie Blakely
 * @version 6-14-18
 */
public interface PigStrategy {
	
	/**
	 * Decides whether to roll the dice again
	 * 
	 * @param  	rollsTaken - number of rolls already taken this turn
	 * @param	turnTotal - number of points rolled so far this turn	
	 * @param	howCloseToGoal - difference between the total points so far and the goal score
	 * @return	true if player should roll again false if not
	 */
	boolean rollAgain(int rollsTaken, int turnTotal, int howCloseToGoal);
	
	/**
	 * Returns strategy name
	 * 
	 * @return	strategy name
	 */
	String getName();
}
