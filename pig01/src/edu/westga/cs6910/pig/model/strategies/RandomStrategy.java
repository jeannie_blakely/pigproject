package edu.westga.cs6910.pig.model.strategies;

/**
 * Implements the game-play strategy that returns true (rolls again) or false (does
 * not roll again) randomly
 * 
 * @author Jeannie Blakely
 * @version 6-15-18
 */
public class RandomStrategy implements PigStrategy {
	/**
	 * Implements PigStrategy's rollAgain() to return either true of false randomly
	 * 
	 */
	@Override
	public boolean rollAgain(int rollsTaken, int turnTotal, int howCloseToGoal) {
		if (howCloseToGoal <= 0) {
			return false;
		} else {
			return Math.random() < 0.5;
		}
	}
	
	/**
	 * 	Returns the strategy name
	 * @return	String
	 */
	@Override
	public String getName() {
		return "Random";
	}
}
