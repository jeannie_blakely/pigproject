package edu.westga.cs6910.pig.model.strategies;

/**
 * Implements the game-play strategy that returns true (rolls again) if player has rolled
 * 3 times or less and false (does not roll again) thereafter 
 * 
 * @author Jeannie Blakely
 * @version 6-15-18
 */
public class GreedyStrategy implements PigStrategy {
	/**
	 * Implements PigStrategy's rollAgain() to return either true if rollsTaken is >
	 * 3 and false otherwise
	 * 
	 */
	@Override
	public boolean rollAgain(int rollsTaken, int turnTotal, int howCloseToGoal) {
		if (howCloseToGoal <= 0) {
			return false;
		} else {
			int rolls = rollsTaken;
			return rolls <= 3;
		}
	}
	
	/**
	 * 	Returns the strategy name
	 * @return	String
	 */
	@Override
	public String getName() {
		return "Greedy";
	}
}
