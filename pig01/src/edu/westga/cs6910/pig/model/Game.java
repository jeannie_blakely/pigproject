package edu.westga.cs6910.pig.model;

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * Game represents a Pig game.
 * @author	CS6910
 * @version	Summer 2018
 * 
 * @author JEANNIE BLAKELY
 * @version 5-31-18
 */
public class Game implements Observable {

	/**
	 * The goal score for the game; commented out to adjust for user input goalScore <jbb 6-21>
	 */
	//public static final int GOAL_SCORE = 100;
	private int goalScore;
	private ObjectProperty<Player> currentPlayerObject;	

	private HumanPlayer theHuman;
	private ComputerPlayer theComputer;
	private DicePair thePair;

	/**
	 * Creates a Pig Game with the specified Players and
	 * a pair of dice and default goalScore of 100
	 * 
	 * @param theComputer	the automated player
	 * @param theHuman		the human player
	 * 
	 * @require				theHuman != null && theComputer != null 
	 * 
	 * @ensure				humanPlayer().equals(theHuman) &&
	 *         				computerPlayer.equals(theComputer)
	 */
	public Game(HumanPlayer theHuman, ComputerPlayer theComputer) {
		this(theHuman, theComputer, 100);
	}
	
	/**
	 * Creates a Pig Game with the specified Players, a pair of dice
	 * and a goal score
	 * 
	 * @param theComputer	the automated player
	 * @param theHuman		the human player
	 * @param theGoalScore			the goal score
	 * 
	 * @require				theHuman != null && theComputer != null 
	 * 
	 * @ensure				humanPlayer().equals(theHuman) &&
	 *         				computerPlayer.equals(theComputer)
	 */
	public Game(HumanPlayer theHuman, ComputerPlayer theComputer, int theGoalScore) {
		if (theHuman == null) {
			throw new IllegalArgumentException("Invalid HumanPlayer; Game not created");
		}
		this.theHuman = theHuman;
		if (theComputer == null) {
			throw new IllegalArgumentException("Invalid ComputerPlayer; Game not created");
		}
		this.theComputer = theComputer;
		if (theGoalScore <= 0) {
			this.goalScore = 100;
		} else {
			this.goalScore = theGoalScore;
		}
		this.currentPlayerObject = new SimpleObjectProperty<Player>();
		this.thePair = new DicePair();
	}

	
	// *********************** mutator methods *************************

	/**
	 * Initializes the game for play.
	 * 
	 * @param firstPlayer 	the Player who takes the first turn
	 * 
	 * @require 			firstPlayer != null && 
	 * 						!firstPlayer.equals(secondPlayer)
	 * 
	 * @ensures 			whoseTurn().equals(firstPlayer) &&
	 * 						firstPlayer.getTotal() == 0
	 */
	public void startNewGame(Player firstPlayer) {
		this.currentPlayerObject.setValue(firstPlayer); 
			
		this.thePair = new DicePair();		
	}

	/**
	 * Conducts a move in the game, allowing the appropriate Player to
	 * take a turn. Has no effect if the game is over.
	 * 
	 * @requires	!isGameOver()
	 * 
	 * @ensures		!whoseTurn().equals(whoseTurn()@prev)
	 */
	public void play() {
		Player currentPlayer = this.currentPlayerObject.getValue();
		this.currentPlayerObject.getValue().takeTurn();
		
		// Force the user interface to update by changing the currentPlayerObject
		//	to null and back
		this.currentPlayerObject.setValue(null);
		this.currentPlayerObject.setValue(currentPlayer);
		
		if (!this.currentPlayerObject.getValue().getIsMyTurn()) {		
			this.hold();
			// moved this method into hold method so each player gets turnTotal reset
			//this.currentPlayerObject.getValue().resetTurnTotal();
		}
	}
	
	/**
	 * Notifies the game that the player is holding
	 * 
	 * @requires	!isGameOver()
	 * 
	 * @ensures		!whoseTurn().equals(whoseTurn()@prev)
	 */
	public void hold() {
		// Swap whose turn it is.
		this.swapWhoseTurn();
		// moved from play method to try to fix turnTotal bug <jbb>
		this.currentPlayerObject.getValue().resetTurnTotal();
		this.isGameOver();
	}
	
	/**
	 * Sets the goal score
	 * @param			theGoalScore input goal score
	 * @precondition	theGoalScore > 0
	 */
	public void setGoalScore(int theGoalScore) {
		if (theGoalScore <= 0) {
			this.goalScore = 100;
		} else {
			this.goalScore = theGoalScore;
		}
	}

	// *********************** accessor methods *************************
	/**
	 * Returns the human Player object.
	 * 
	 * @return the human Player
	 */
	public int getGoalScore() {
		return this.goalScore;
	}
	
	/**
	 * Returns the human Player object.
	 * 
	 * @return the human Player
	 */
	public HumanPlayer getHumanPlayer() {
		return this.theHuman;
	}

	/**
	 * Returns the computer Player object.
	 * 
	 * @return the computer Player
	 */
	public ComputerPlayer getComputerPlayer() {
		return this.theComputer;
	}
	
	/**
	 * Returns the Player whose turn it is.
	 * 
	 * @return	the current Player
	 */
	public Player getCurrentPlayer() {
		return this.currentPlayerObject.getValue();
	}

	/**
	 * Return whether the game is over.
	 * 
	 * @return true iff currentPlayer.getTotal() >= GOAL_SCORE
	 */
	public boolean isGameOver() {	
		//System.out.println(this.currentPlayerObject.getValue().getTotal());
		if (this.currentPlayerObject.getValue() == null) {
			return true;
		}
		
		//if (this.currentPlayerObject.getValue().getTotal() >= GOAL_SCORE) {
		if (this.getComputerPlayer().getTotal() >= this.goalScore || this.getHumanPlayer().getTotal() >= this.goalScore) {
			return true;
		}
		return false;
	}
	
	/** 
	 * Returns the pair of dice being used in the game
	 * 
	 * @return	the pair of dice
	 */
	public DicePair getDicePair() {
		return this.thePair;
	}

	/**
	 * Returns a String showing the goal score, or, if
	 * the game is over, the name of the winner.
	 * 
	 * @return a String representation of this Game
	 */
	public String toString() {	
		String result = "Goal Score: " + this.goalScore;
		result += System.getProperty("line.separator")
				+ this.theHuman.getName() + ": "
				+ this.theHuman.getTotal();
		result += System.getProperty("line.separator")
				+ this.theComputer.getName() + ": "
						+ this.theComputer.getTotal();
		
		//if (!this.isGameOver()) {
		//	return result;
		//}
		
		if (this.theHuman.getTotal() >= this.goalScore) {
			return result + System.getProperty("line.separator")
					+ "Game over! Winner: " + this.theHuman.getName();
		} else if (this.theComputer.getTotal() >= this.goalScore) {
			return result + System.getProperty("line.separator")
					+ "Game over! Winner: " + this.theComputer.getName();
		} else {
			return result;
		}
	}

	
	
	// ************************ private helper method *************************

	private void swapWhoseTurn() {
		// Swap the players so that the other player becomes 
		// the current player.  Note that in order to access the
		// object inside of the ObjectProperty, you'll need to use
		// getValue() and setValue()
		if (this.currentPlayerObject.getValue() == this.theHuman) {
			this.currentPlayerObject.setValue(this.theComputer);
		} else {
			this.currentPlayerObject.setValue(this.theHuman);
		}
	}

	@Override
	public void addListener(InvalidationListener theListener) {
		this.currentPlayerObject.addListener(theListener);
	}

	@Override
	public void removeListener(InvalidationListener theListener) {
		this.currentPlayerObject.removeListener(theListener);
	}
	
}
